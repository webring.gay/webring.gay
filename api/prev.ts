import {
	VercelRequest as Request,
	VercelResponse as Response,
} from "@vercel/node";
import { readSites } from "../lib/data";
import { findCurrentSite } from "../lib/findCurrentSite";
import { getRandomSite } from "../lib/randomSite";

export default async function prevSite(req: Request, res: Response) {
	const sites = await readSites();
	if (!sites) throw new Error("webring is misconfigued");

	const currentIndex: number | null = await findCurrentSite(req, sites);

	const targetSite =
		currentIndex !== null
			? sites[(currentIndex + sites.length - 1) % sites.length]
			: getRandomSite(sites);

	console.log({
		sites,
		currentIndex,
		query: req.query.site,
		referer: req.headers.referer,
		targetSite,
	});

	res.redirect(307, targetSite);
}

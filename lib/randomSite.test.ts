import { mockRandom, resetMockRandom } from "jest-mock-random";
import { getRandomSite } from "./randomSite";

const SITES = [
	"https://emma.cafe/",
	"https://oliviaartz.com/",
	"https://punx.us/",
];

describe("randomSite", () => {
	afterEach(resetMockRandom);
	it("picks a random site when coming from undefined site", () => {
		mockRandom(1 / SITES.length);
		const site = getRandomSite(SITES, null);
		expect(site).toEqual(SITES[1]);
	});

	it("picks a random site when coming from known site (0)", () => {
		mockRandom(0 / SITES.length);
		const site = getRandomSite(SITES, 0);
		expect(site).toEqual(SITES[1]);
	});

	it("picks a random site when coming from known site (1)", () => {
		mockRandom(1 / (SITES.length - 1));
		const site = getRandomSite(SITES, 1);
		expect(site).toEqual(SITES[2]);
	});

	it("picks a random site when coming from known site (2)", () => {
		mockRandom(0.9999999999999999);
		const site = getRandomSite(SITES, 2);
		expect(site).toEqual(SITES[1]);
	});
});

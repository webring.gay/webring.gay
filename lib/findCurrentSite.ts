import type { VercelRequest as Request } from "@vercel/node";
import normalizeUrl from "normalize-url";

const normalize = (url: string) =>
	normalizeUrl(url, { stripProtocol: true, stripHash: true });

export async function findCurrentSite(
	req: Pick<Request, "query" | "headers">,
	sites: string[]
) {
	// get first site from query string, if supplied
	const querySite = Array.isArray(req.query.site)
		? req.query.site[0]
		: req.query.site;
	const currentSite = querySite || req.headers.referer || "";
	let currentIndex = -1;
	try {
		currentIndex = sites.findIndex(
			(site) => normalize(site) === normalize(currentSite)
		);
	} catch (error) {
		return null;
	}

	if (currentIndex === -1) {
		return null;
	} else {
		return currentIndex;
	}
}

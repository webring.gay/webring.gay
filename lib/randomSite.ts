export function getRandomSite(
	sites: string[],
	currentIndex: number | null = null
) {
	const hasCurrentSite = currentIndex !== null;
	let rand = Math.floor(
		Math.random() * (sites.length - (hasCurrentSite ? 1 : 0))
	);
	if (currentIndex !== null && rand >= currentIndex) rand++;
	return sites[rand];
}

import { parse } from "toml";
import fs from "fs/promises";
import path from "path";

export async function readSites(): Promise<string[] | undefined> {
	const config = parse(
		String(
			await fs.readFile(path.resolve(process.cwd(), "data/sites.toml"), "utf-8")
		)
	);
	const sites = config.sites;
	return sites;
}

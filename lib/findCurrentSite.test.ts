import { findCurrentSite } from "./findCurrentSite";

const SITES = [
	"https://emma.cafe/",
	"https://oliviaartz.com/",
	"https://punx.us/",
];

describe("findCurrentSite", () => {
	it("finds a url that matches exactly in referer", async () => {
		const currentSite = await findCurrentSite(
			{ query: {}, headers: { referer: "https://emma.cafe" } },
			SITES
		);

		expect(currentSite).toBe(0);
	});

	it("finds a url that matches exactly in query", async () => {
		const currentSite = await findCurrentSite(
			{
				query: { site: "https://oliviaartz.com/" },
				headers: {},
			},
			SITES
		);

		expect(currentSite).toBe(1);
	});

	it("finds a url that can be normalized", async () => {
		const currentSite = await findCurrentSite(
			{
				query: { site: "http://www.emma.cafe/#test" },
				headers: {},
			},
			SITES
		);

		expect(currentSite).toBe(0);
	});

	it("returns null url when current site can't be found", async () => {
		const currentSite = await findCurrentSite(
			{ query: {}, headers: {} },
			SITES
		);

		expect(currentSite).toBe(null);
	});

	it("returns null url when current site exists but isn't in webring", async () => {
		const currentSite = await findCurrentSite(
			{ query: {}, headers: { referer: "https://google.com" } },
			SITES
		);

		expect(currentSite).toBe(null);
	});

	it("returns uses the site in the query before the referer header", async () => {
		const currentSite = await findCurrentSite(
			{
				query: { site: "https://punx.us/" },
				headers: { referer: "https://oliviaartz.com/" },
			},
			SITES
		);

		expect(currentSite).toBe(2);
	});
});
